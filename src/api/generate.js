import request from '@/utils/request'

export default {
  parse(param){
    return request({
      url:'/gen/parse',
      method:'post',
      data: param,
      headers:{
        'Content-Type':'multipart/form-data'
      }
    })
  },
  doGenerateByFile(param){
    return request({
      url: '/gen/doGenByFile',
      method: 'post',
      data: param,
      responseType: 'blob',
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },
  doGenerateByTable(data){
    return request({
      url:'/gen/doGenByTable',
      method:'post',
      data
    })
  }
}
