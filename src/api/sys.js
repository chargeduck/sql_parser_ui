import request from '@/utils/request'

export default {
  captchaImg(){
    return request({
      url:'/sys/captcha',
      method:'get'
    })
  }
}
